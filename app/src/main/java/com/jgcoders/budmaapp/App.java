package com.jgcoders.budmaapp;

import android.app.Application;

import com.jgcoders.budmaapp.dagger2.Components.DaggerNetworkComponent;
import com.jgcoders.budmaapp.dagger2.Components.NetworkComponent;
import com.jgcoders.budmaapp.dagger2.Modules.AppModule;
import com.jgcoders.budmaapp.dagger2.Modules.NetworkModule;

/**
 * Created by krzysztofm on 26.01.2018.
 */

public class App extends Application {

    private NetworkComponent mNetworkComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mNetworkComponent = DaggerNetworkComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule("http://psd-restapi.internet-rzeczy.net/"))
                .build();
    }

    public NetworkComponent getNetworkComponent() {
        return mNetworkComponent;
    }
}
