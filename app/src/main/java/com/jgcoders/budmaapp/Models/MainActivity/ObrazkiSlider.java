package com.jgcoders.budmaapp.Models.MainActivity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by staszekalcatraz on 27/01/2018.
 */

public class ObrazkiSlider implements Serializable {
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("opis")
    @Expose
    private String opis;
    @SerializedName("akcja")
    @Expose
    private String akcja;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getAkcja() {
        return akcja;
    }

    public void setAkcja(String akcja) {
        this.akcja = akcja;
    }
}
