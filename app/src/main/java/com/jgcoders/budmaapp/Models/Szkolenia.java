package com.jgcoders.budmaapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class Szkolenia {
    @SerializedName("nazwa")
    @Expose
    private String nazwa;
    @SerializedName("data")
    @Expose
    private String data;
    @SerializedName("opis")
    @Expose
    private String opis;

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

}
