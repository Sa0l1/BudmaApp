package com.jgcoders.budmaapp.Models.MainActivity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by staszekalcatraz on 27/01/2018.
 */

public class HityCenowe implements Serializable{
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("nazwa")
    @Expose
    private String nazwa;
    @SerializedName("cena")
    @Expose
    private String cena;
    @SerializedName("akcja")
    @Expose
    private String akcja;


    public HityCenowe(String logo, String nazwa, String cena, String akcja) {
        this.logo = logo;
        this.nazwa = nazwa;
        this.cena = cena;
        this.akcja = akcja;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    public String getAkcja() {
        return akcja;
    }

    public void setAkcja(String akcja) {
        this.akcja = akcja;
    }
}
