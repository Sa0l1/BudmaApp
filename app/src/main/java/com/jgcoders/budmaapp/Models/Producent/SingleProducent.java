package com.jgcoders.budmaapp.Models.Producent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by staszekalcatraz on 27/01/2018.
 */

public class SingleProducent {
    @SerializedName("nazwa")
    @Expose
    private String nazwa;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("adres")
    @Expose
    private String adres;
    @SerializedName("telefon")
    @Expose
    private String telefon;
    @SerializedName("produkty")
    @Expose
    private List<Produkty> produkty = null;

    @SerializedName("akcja")
    @Expose
    private String akcja;

    public String getAkcja() {
        return akcja;
    }

    public void setAkcja(String akcja) {
        this.akcja = akcja;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public List<Produkty> getProdukty() {
        return produkty;
    }

    public void setProdukty(List<Produkty> produkty) {
        this.produkty = produkty;
    }
}
