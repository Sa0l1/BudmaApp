package com.jgcoders.budmaapp.Models.Producent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by krzysztofm on 26.01.2018.
 */

public class ProduktModel{
    @SerializedName("zdjecie")
    @Expose
    private String zdjecie;
    @SerializedName("nazwa")
    @Expose
    private String nazwa;
    @SerializedName("nrRef")
    @Expose
    private String nrRef;
    @SerializedName("cena")
    @Expose
    private String cena;
    @SerializedName("opis")
    @Expose
    private String opis;

    public String getZdjecie() {
        return zdjecie;
    }

    public void setZdjecie(String zdjecie) {
        this.zdjecie = zdjecie;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getNrRef() {
        return nrRef;
    }

    public void setNrRef(String nrRef) {
        this.nrRef = nrRef;
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

}
