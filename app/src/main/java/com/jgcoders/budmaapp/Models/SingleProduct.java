package com.jgcoders.budmaapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class SingleProduct {
    @SerializedName("nazwa")
    @Expose
    private String nazwa;
    @SerializedName("zdjecia")
    @Expose
    private List<String> zdjecia = null;
    @SerializedName("akcjaHurtownii")
    @Expose
    private String akcjaHurtownii;
    @SerializedName("nrRef")
    @Expose
    private String nrRef;
    @SerializedName("cena")
    @Expose
    private String cena;
    @SerializedName("opis")
    @Expose
    private String opis;

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public List<String> getZdjecia() {
        return zdjecia;
    }

    public void setZdjecia(List<String> zdjecia) {
        this.zdjecia = zdjecia;
    }

    public String getAkcjaHurtownii() {
        return akcjaHurtownii;
    }

    public void setAkcjaHurtownii(String akcjaHurtownii) {
        this.akcjaHurtownii = akcjaHurtownii;
    }

    public String getNrRef() {
        return nrRef;
    }

    public void setNrRef(String nrRef) {
        this.nrRef = nrRef;
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
}
