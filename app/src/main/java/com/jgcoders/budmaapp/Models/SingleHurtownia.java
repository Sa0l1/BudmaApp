package com.jgcoders.budmaapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.Producent.Produkty;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class SingleHurtownia {
    @SerializedName("nazwa")
    @Expose
    private String nazwa;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("adres")
    @Expose
    private String adres;
    @SerializedName("telefon")
    @Expose
    private String telefon;
    @SerializedName("produkty")
    @Expose
    private List<Produkty> produkty = null;

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public List<Produkty> getProdukty() {
        return produkty;
    }

    public void setProdukty(List<Produkty> produkty) {
        this.produkty = produkty;
    }

}
