package com.jgcoders.budmaapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class Listainwestycji {
    @SerializedName("miniaturka")
    @Expose
    private String miniaturka;
    @SerializedName("nazwa")
    @Expose
    private String nazwa;
    @SerializedName("opis")
    @Expose
    private String opis;
    @SerializedName("akcja")
    @Expose
    private String akcja;

    public String getMiniaturka() {
        return miniaturka;
    }

    public void setMiniaturka(String miniaturka) {
        this.miniaturka = miniaturka;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getAkcja() {
        return akcja;
    }

    public void setAkcja(String akcja) {
        this.akcja = akcja;
    }
}
