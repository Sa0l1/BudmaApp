package com.jgcoders.budmaapp.Models.Producent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by krzysztofm on 26.01.2018.
 */

public class ListaProducentowModel {
    @SerializedName("nazwa")
    @Expose
    private String nazwa;
    @SerializedName("adres")
    @Expose
    private String adres;
    @SerializedName("telefon")
    @Expose
    private String telefon;
    @SerializedName("www")
    @Expose
    private String www;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("akcja")
    @Expose
    private String akcja;

    public String getAkcja() {
        return akcja;
    }

    public void setAkcja(String akcja) {
        this.akcja = akcja;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }
}
