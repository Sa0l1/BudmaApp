package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.MainActivity.HityCenowe;
import com.jgcoders.budmaapp.Models.MainActivity.ObrazkiSlider;

import java.util.List;

/**
 * Created by staszekalcatraz on 27/01/2018.
 */

public class MainActivityJsonResponse {
    @SerializedName("ObrazkiSlider")
    @Expose
    private List<ObrazkiSlider> obrazkiSlider = null;
    @SerializedName("HityCenowe")
    @Expose
    private List<HityCenowe> hityCenowe = null;

    public List<ObrazkiSlider> getObrazkiSlider() {
        return obrazkiSlider;
    }

    public void setObrazkiSlider(List<ObrazkiSlider> obrazkiSlider) {
        this.obrazkiSlider = obrazkiSlider;
    }

    public List<HityCenowe> getHityCenowe() {
        return hityCenowe;
    }

    public void setHityCenowe(List<HityCenowe> hityCenowe) {
        this.hityCenowe = hityCenowe;
    }
}
