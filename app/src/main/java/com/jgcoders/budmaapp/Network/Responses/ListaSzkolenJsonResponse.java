package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.Szkolenia;

import java.io.Serializable;
import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class ListaSzkolenJsonResponse implements Serializable {
    @SerializedName("Szkolenia")
    @Expose
    private List<Szkolenia> szkolenia = null;

    public List<Szkolenia> getSzkolenia() {
        return szkolenia;
    }

    public void setSzkolenia(List<Szkolenia> szkolenia) {
        this.szkolenia = szkolenia;
    }
}
