package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.Producent.SingleProducent;

/**
 * Created by staszekalcatraz on 27/01/2018.
 */

public class SingleProducentJsonResponse {
    @SerializedName("singleProducent")
    @Expose
    private SingleProducent singleProducent;

    public SingleProducent getSingleProducent() {
        return singleProducent;
    }

    public void setSingleProducent(SingleProducent singleProducent) {
        this.singleProducent = singleProducent;
    }
}
