package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.Singleinwestycja;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class SingleInwestycjaJsonResponse {
    @SerializedName("singleinwestycja")
    @Expose
    private Singleinwestycja singleinwestycja;

    public Singleinwestycja getSingleinwestycja() {
        return singleinwestycja;
    }

    public void setSingleinwestycja(Singleinwestycja singleinwestycja) {
        this.singleinwestycja = singleinwestycja;
    }
}
