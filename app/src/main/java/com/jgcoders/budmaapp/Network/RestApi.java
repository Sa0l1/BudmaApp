package com.jgcoders.budmaapp.Network;

import com.jgcoders.budmaapp.Models.Singleinwestycja;
import com.jgcoders.budmaapp.Network.Responses.ListaHurtowniJsonResponse;
import com.jgcoders.budmaapp.Network.Responses.ListaInwestycjiJsonResponse;
import com.jgcoders.budmaapp.Network.Responses.ListaProducentowJsonResponse;
import com.jgcoders.budmaapp.Network.Responses.ListaSzkolenJsonResponse;
import com.jgcoders.budmaapp.Network.Responses.MainActivityJsonResponse;
import com.jgcoders.budmaapp.Network.Responses.SingleHurtowniaJsonResponse;
import com.jgcoders.budmaapp.Network.Responses.SingleInwestycjaJsonResponse;
import com.jgcoders.budmaapp.Network.Responses.SingleProducentJsonResponse;
import com.jgcoders.budmaapp.Network.Responses.SingleProductJsonResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by krzysztofm on 26.01.2018.
 */

public interface RestApi {

    @POST("mainactivity.php")
    Call<MainActivityJsonResponse> getMainActivity();

    @POST("listaproducentow.php")
    Call<ListaProducentowJsonResponse> getListaProducentow();

    @FormUrlEncoded
    @POST("singleproduct.php")
    Call<SingleProductJsonResponse> getSingleProduct(@Field("akcja") String akcja);

    @FormUrlEncoded
    @POST("singleproducent.php")
    Call<SingleProducentJsonResponse> getSingleProducent(@Field("akcja") String akcja);

    /*@GET("promocje")
    Call<PromocjeJsonResponse> getPromocje();*/

    @POST("szkolenia")
    Call<ListaSzkolenJsonResponse> getSzkolenia();

    @POST("listahurtowni.php")
    Call<ListaHurtowniJsonResponse> getListaHurtownii();

    @FormUrlEncoded
    @POST("singlehurtownia.php")
    Call<SingleHurtowniaJsonResponse> getSingleHurtownia(@Field("akcja") String akcja);

    @POST("listainwestycji.php")
    Call<ListaInwestycjiJsonResponse> getListaInwestycji();

    @FormUrlEncoded
    @POST("singleinwestycja.php")
    Call<SingleInwestycjaJsonResponse> getSingleInwestycja(@Field("akcja") String akcja);

}
