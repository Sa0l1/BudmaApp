package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.SingleHurtownia;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class SingleHurtowniaJsonResponse {
    @SerializedName("singlehurtownia")
    @Expose
    private SingleHurtownia singlehurtownia;

    public SingleHurtownia getSinglehurtownia() {
        return singlehurtownia;
    }

    public void setSinglehurtownia(SingleHurtownia singlehurtownia) {
        this.singlehurtownia = singlehurtownia;
    }

}
