package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.Listainwestycji;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class ListaInwestycjiJsonResponse {
    @SerializedName("listainwestycji")
    @Expose
    private List<Listainwestycji> listainwestycji = null;

    public List<Listainwestycji> getListainwestycji() {
        return listainwestycji;
    }

    public void setListainwestycji(List<Listainwestycji> listainwestycji) {
        this.listainwestycji = listainwestycji;
    }
}
