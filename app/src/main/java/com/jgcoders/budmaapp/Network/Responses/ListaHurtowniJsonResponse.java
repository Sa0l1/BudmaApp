package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.ListaHurtowni;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class ListaHurtowniJsonResponse {
    @SerializedName("ListaHurtownii")
    @Expose
    private List<ListaHurtowni> listaHurtownii = null;

    public List<ListaHurtowni> getListaHurtownii() {
        return listaHurtownii;
    }

    public void setListaHurtownii(List<ListaHurtowni> listaHurtownii) {
        this.listaHurtownii = listaHurtownii;
    }
}
