package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.Producent.ListaProducentowModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by staszekalcatraz on 27/01/2018.
 */

public class ListaProducentowJsonResponse implements Serializable {
    @SerializedName("listaProducentow")
    @Expose
    private List<ListaProducentowModel> listaProducentow = null;

    public List<ListaProducentowModel> getListaProducentow() {
        return listaProducentow;
    }

    public void setListaProducentow(List<ListaProducentowModel> listaProducentow) {
        this.listaProducentow = listaProducentow;
    }

}
