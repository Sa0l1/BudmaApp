package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.Promocje;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class PromocjeJsonResponse {
    @SerializedName("Promocje")
    @Expose
    private List<Promocje> promocje = null;

    public List<Promocje> getPromocje() {
        return promocje;
    }

    public void setPromocje(List<Promocje> promocje) {
        this.promocje = promocje;
    }
}
