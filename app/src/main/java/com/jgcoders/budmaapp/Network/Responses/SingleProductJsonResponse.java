package com.jgcoders.budmaapp.Network.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jgcoders.budmaapp.Models.SingleProduct;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class SingleProductJsonResponse {
    @SerializedName("singleProduct")
    @Expose
    private SingleProduct singleProduct;

    public SingleProduct getSingleProduct() {
        return singleProduct;
    }

    public void setSingleProduct(SingleProduct singleProduct) {
        this.singleProduct = singleProduct;
    }
}
