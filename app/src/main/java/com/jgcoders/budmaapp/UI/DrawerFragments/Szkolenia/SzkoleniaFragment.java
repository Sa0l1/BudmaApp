package com.jgcoders.budmaapp.UI.DrawerFragments.Szkolenia;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Models.Szkolenia;
import com.jgcoders.budmaapp.UI.DrawerFragments.Adapters.ListaSzkolenDataAdapter;
import com.jgcoders.budmaapp.Network.Responses.ListaSzkolenJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SzkoleniaFragment extends Fragment {

    @Inject
    Retrofit retrofit;

    private RecyclerView rvSzkolenia;

    public SzkoleniaFragment() {
        // Required empty public constructor
    }

    public static SzkoleniaFragment newInstance() {
        SzkoleniaFragment fragment = new SzkoleniaFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_szkolenia, container, false);
        ((App) getActivity().getApplication()).getNetworkComponent().inject(this);
        rvSzkolenia = view.findViewById(R.id.rvSzkolenia);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvSzkolenia.setHasFixedSize(true);
        rvSzkolenia.setLayoutManager(layoutManager);
        loadRecyclerViewData();

        return view;
    }

    private void loadRecyclerViewData() {
         RestApi restApi = retrofit.create(RestApi.class);
        Call<ListaSzkolenJsonResponse> call = restApi.getSzkolenia();
        call.enqueue(new Callback<ListaSzkolenJsonResponse>() {
            @Override
            public void onResponse(Call<ListaSzkolenJsonResponse> call, Response<ListaSzkolenJsonResponse> response) {
                List<Szkolenia> listaSzkolen = response.body().getSzkolenia();
                rvSzkolenia.setAdapter(new ListaSzkolenDataAdapter(listaSzkolen, getActivity(), R.layout.szkolenia_row));
            }

            @Override
            public void onFailure(Call<ListaSzkolenJsonResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
