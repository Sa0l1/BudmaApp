package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.Models.SingleProduct;
import com.jgcoders.budmaapp.R;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class SingleProductDataAdapter extends RecyclerView.Adapter<SingleProductDataAdapter.ViewHolder> {
    private SingleProduct singleProducts;
    private Context context;
    private int rowLayout;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_single_product, parent, false);
        return new SingleProductDataAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvNazwa.setText(singleProducts.getNazwa());
        holder.tvNrRef.setText(singleProducts.getNrRef());
        holder.tvCena.setText(singleProducts.getCena());
        holder.tvOpis.setText(singleProducts.getOpis());
        final String akcjaDoHurtownii = singleProducts.getAkcjaHurtownii();
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivSingleProductImage;
        private TextView tvNazwa, tvNrRef, tvCena, tvOpis, tvSprawdzHurtownie;


        public ViewHolder(View itemView) {
            super(itemView);

            tvNazwa = itemView.findViewById(R.id.tvSingleProductTitle);
            tvNrRef = itemView.findViewById(R.id.tvNrRef);
            tvCena = itemView.findViewById(R.id.tvCena);
            //tvOpis = itemView.findViewById(R.id.tvOpis);

            tvSprawdzHurtownie = itemView.findViewById(R.id.tvSprawdzHurtownie);

        }
    }
}
