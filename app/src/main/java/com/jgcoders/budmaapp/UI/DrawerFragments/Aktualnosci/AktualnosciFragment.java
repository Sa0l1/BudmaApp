package com.jgcoders.budmaapp.UI.DrawerFragments.Aktualnosci;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Models.MainActivity.HityCenowe;
import com.jgcoders.budmaapp.Network.Responses.MainActivityJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.Adapters.HityCenoweDataAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AktualnosciFragment extends Fragment {

    @Inject
    Retrofit retrofit;

    private SliderLayout mSliderLayout;

    private RecyclerView rvHityCenowe;

    public AktualnosciFragment() {
        // Required empty public constructor
    }


    public static AktualnosciFragment newInstance() {
        AktualnosciFragment fragment = new AktualnosciFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_aktualnosci, container, false);
        ((App) getActivity().getApplication()).getNetworkComponent().inject(this);

        ArrayList<String> listName = new ArrayList<>();

        mSliderLayout = view.findViewById(R.id.sliderLayout);

        rvHityCenowe = view.findViewById(R.id.rvHityCenowe);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvHityCenowe.setHasFixedSize(true);
        rvHityCenowe.setLayoutManager(layoutManager);
        loadRecyclerViewData();

        /*listUrl.add("https://upload.wikimedia.org/wikipedia/commons/5/5b/7_15_BroadPeak.jpg");
        listUrl.add("https://tctechcrunch2011.files.wordpress.com/2017/02/android-studio-logo.png");
        listUrl.add("http://static.tumblr.com/7650edd3fb8f7f2287d79a67b5fec211/3mg2skq/3bdn278j2/tumblr_static_idk_what.gif");*/

        return view;
    }

    private void loadRecyclerViewData() {
        RestApi restApi = retrofit.create(RestApi.class);

        Call<MainActivityJsonResponse> call = restApi.getMainActivity();
        call.enqueue(new Callback<MainActivityJsonResponse>() {
            @Override
            public void onResponse(Call<MainActivityJsonResponse> call, Response<MainActivityJsonResponse> response) {
                List<HityCenowe> hityCenowes = response.body().getHityCenowe();
                rvHityCenowe.setAdapter(new HityCenoweDataAdapter(hityCenowes, getActivity().getApplicationContext(), R.layout.hity_cenowe_row));

                Log.e("obrazki slider size", "" + response.body().getObrazkiSlider().size());
                //Log.e("obrazki slider ", "" + response.body().getObrazkiSlider().get(i).getUrl());
                ArrayList<String> listUrl = new ArrayList<>();

                for (int i = 0; i < response.body().getObrazkiSlider().size(); i++) {
                    String obrazek = response.body().getObrazkiSlider().get(i).getUrl();
                    listUrl.add(obrazek);
                }
                RequestOptions requestOptions = new RequestOptions();
                requestOptions
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE);

                for (int i = 0; i < listUrl.size(); i++) {
                    TextSliderView sliderView = new TextSliderView(getActivity());
                    // if you want show image only / without description text use DefaultSliderView instead

                    // initialize SliderLayout
                    sliderView.image(listUrl.get(i))
                            //.description(listName.get(i))
                            .setRequestOption(requestOptions)
                            .setBackgroundColor(Color.WHITE)
                            .setProgressBarVisible(false);
                    //.setOnSliderClickListener(getActivity());

                    //add your extra information
                    sliderView.bundle(new Bundle());
                    //sliderView.getBundle().putString("extra", listName.get(i));
                    mSliderLayout.addSlider(sliderView);
                }

                // set Slider Transition Animation
                // mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);

                mSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                mSliderLayout.setCustomAnimation(new DescriptionAnimation());
                mSliderLayout.setDuration(4000);
                //mSliderLayout.addOnPageChangeListener(MainActivity);
            }

            @Override
            public void onFailure(Call<MainActivityJsonResponse> call, Throwable t) {
                Log.e("blad", "" + t);
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
