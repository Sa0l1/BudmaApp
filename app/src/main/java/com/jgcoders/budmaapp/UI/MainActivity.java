package com.jgcoders.budmaapp.UI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;
import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.Aktualnosci.AktualnosciFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.DzialObslugiKontakt.KontaktFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.Hurtownie.ListaHurtowniiFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.InwestycjeFragment.InwestycjeFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.Producenci.ProducenciFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.Promocje.PromocjeFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.Szkolenia.SzkoleniaFragment;
import com.jgcoders.budmaapp.UI.LoginActivity.LoginActivity;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    @Inject
    Retrofit retrofit;

    private Boolean isLoggedin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((App) getApplication()).getNetworkComponent().inject(this);

        //sprawdz czy uzytkownik jest zalogowany
        SharedPreferences sharedPreferencesUserLogin = getSharedPreferences("userLogin", Context.MODE_PRIVATE);
        isLoggedin = sharedPreferencesUserLogin.getBoolean("", false);

        if (!isLoggedin) {
            startActivity(new Intent(this, LoginActivity.class));
            Log.e("logowanie", "nie zalogowany");
            return;
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, AktualnosciFragment.newInstance());
        fragmentTransaction.commit();

        Toolbar toolbar = findViewById(R.id.toolbar);
        this.setTitle("");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            SharedPreferences sharedPreferencesUserLogin = getSharedPreferences("userLogin", Context.MODE_PRIVATE);
            SharedPreferences.Editor sharedPrefEditor = sharedPreferencesUserLogin.edit();
            sharedPrefEditor.clear();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment selectedFragment = AktualnosciFragment.newInstance();

        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_producenci: {
                selectedFragment = ProducenciFragment.newInstance();

                break;
            }

            case R.id.nav_promocje: {
                selectedFragment = PromocjeFragment.newInstance();

                break;
            }
            case R.id.nav_aktualnosci: {
                selectedFragment = AktualnosciFragment.newInstance();

                break;
            }
            /*case R.id.nav_szkolenia: {
                selectedFragment = SzkoleniaFragment.newInstance();

                break;
            }*/
            case R.id.nav_inwestycje: {
                selectedFragment = InwestycjeFragment.newInstance();

                break;
            }
            case R.id.nav_hurtownie: {
                selectedFragment = ListaHurtowniiFragment.newInstance();

                break;
            }
            case R.id.nav_dzial_obslugi: {
                selectedFragment = KontaktFragment.newInstance();

                break;
            }
        }

        //open fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, selectedFragment);
        fragmentTransaction.commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {
        Toast.makeText(this, baseSliderView.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
