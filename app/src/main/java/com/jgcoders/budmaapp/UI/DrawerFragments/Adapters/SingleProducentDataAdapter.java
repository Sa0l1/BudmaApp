package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.Models.Producent.Produkty;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.SingleProductActivity.SingleProductActivity;

import java.util.List;

/**
 * Created by staszekalcatraz on 27/01/2018.
 */

public class SingleProducentDataAdapter extends RecyclerView.Adapter<SingleProducentDataAdapter.ViewHolder> {
    private List<Produkty> singleProducents;
    private Context context;
    private int rowLayout;

    public SingleProducentDataAdapter(List<Produkty> singleProducents, Context context, int rowLayout) {
        this.singleProducents = singleProducents;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public SingleProducentDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hity_cenowe_row, parent, false);

        return new SingleProducentDataAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SingleProducentDataAdapter.ViewHolder holder, int position) {
        holder.tvHitCenowyNazwa.setText(singleProducents.get(position).getNazwa());
        holder.tvHitCenowyCena.setText(singleProducents.get(position).getCena());
        String ivIco = singleProducents.get(position).getZdjecie();
        final String akcja = singleProducents.get(position).getAkcja();

        Glide.with(context)
                .load(ivIco)
                .into(holder.ivHitCenowyLogo);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SingleProductActivity.class);
                intent.putExtra("akcja", akcja);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return singleProducents.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvHitCenowyNazwa, tvHitCenowyCena;
        private ImageView ivHitCenowyLogo;

        public ViewHolder(View itemView) {
            super(itemView);

            tvHitCenowyNazwa = itemView.findViewById(R.id.tvHitCenowyNazwa);
            tvHitCenowyCena = itemView.findViewById(R.id.tvHitCenowyCena);
            ivHitCenowyLogo = itemView.findViewById(R.id.ivHitCenowyLogo);
        }
    }
}
