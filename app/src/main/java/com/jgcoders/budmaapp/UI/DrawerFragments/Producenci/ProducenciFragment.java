package com.jgcoders.budmaapp.UI.DrawerFragments.Producenci;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Models.Producent.ListaProducentowModel;
import com.jgcoders.budmaapp.UI.DrawerFragments.Adapters.ProducenciListDataAdapter;
import com.jgcoders.budmaapp.Network.Responses.ListaProducentowJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ProducenciFragment extends Fragment {

    @Inject
    Retrofit retrofit;

    private RecyclerView rvListaProducentow;

    public ProducenciFragment() {
        // Required empty public constructor
    }

    public static ProducenciFragment newInstance() {
        ProducenciFragment fragment = new ProducenciFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_producenci, container, false);
        ((App) getActivity().getApplication()).getNetworkComponent().inject(this);

        rvListaProducentow = view.findViewById(R.id.rvListaProducentow);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvListaProducentow.setHasFixedSize(true);
        rvListaProducentow.setLayoutManager(layoutManager);
        loadRecyclerViewData();

        return view;
    }

    private void loadRecyclerViewData() {
        /*Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://psd-restapi.internet-rzeczy.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();*/
        RestApi restApi = retrofit.create(RestApi.class);
        Call<ListaProducentowJsonResponse> call = restApi.getListaProducentow();
        call.enqueue(new Callback<ListaProducentowJsonResponse>() {
            @Override
            public void onResponse(Call<ListaProducentowJsonResponse> call, Response<ListaProducentowJsonResponse> response) {
                Log.e("response", "" + response.body().getListaProducentow());
                List<ListaProducentowModel> listaProducentowModels = response.body().getListaProducentow();
                rvListaProducentow.setAdapter(new ProducenciListDataAdapter(listaProducentowModels, getActivity(), R.layout.producent_row));
            }

            @Override
            public void onFailure(Call<ListaProducentowJsonResponse> call, Throwable t) {
                Log.e("error", "" + t.getMessage());
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
