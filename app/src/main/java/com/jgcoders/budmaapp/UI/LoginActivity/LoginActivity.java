package com.jgcoders.budmaapp.UI.LoginActivity;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.Aktualnosci.AktualnosciFragment;
import com.jgcoders.budmaapp.UI.LoginActivity.AktywacjaFragment.AktywacjaFragment;
import com.jgcoders.budmaapp.UI.LoginActivity.LoginFragment.LoginFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, LoginFragment.newInstance());
        fragmentTransaction.commit();
    }

    @OnClick(R.id.bOpenRegisterFragment)
    public void onOpenRegisterFragmentClick() {
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, AktywacjaFragment.newInstance());
        fragmentTransaction.commit();
    }

    @OnClick(R.id.bOpenZalogujFragment)
    public void onOpenZalogujFragmentClick() {
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, LoginFragment.newInstance());
        fragmentTransaction.commit();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
