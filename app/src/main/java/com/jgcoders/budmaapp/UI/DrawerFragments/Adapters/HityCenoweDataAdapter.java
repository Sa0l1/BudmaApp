package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.Models.MainActivity.HityCenowe;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.SingleProductActivity.SingleProductActivity;

import java.util.List;

/**
 * Created by staszekalcatraz on 27/01/2018.
 */

public class HityCenoweDataAdapter extends RecyclerView.Adapter<HityCenoweDataAdapter.ViewHolder> {
    private List<HityCenowe> hityCenowes;
    private Context context;
    private int rowLayout;

    public HityCenoweDataAdapter(List<HityCenowe> hityCenowes, Context context, int rowLayout) {
        this.hityCenowes = hityCenowes;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public HityCenoweDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hity_cenowe_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HityCenoweDataAdapter.ViewHolder holder, final int position) {
        holder.tvHitCenowyNazwa.setText(hityCenowes.get(position).getNazwa());
        holder.tvHitCenowyCena.setText(hityCenowes.get(position).getCena());
        final String akcja = hityCenowes.get(position).getAkcja();
        String ivIco = hityCenowes.get(position).getLogo();

        Glide.with(context)
                .load(ivIco)
                .into(holder.ivIco);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SingleProductActivity.class);
                intent.putExtra("akcja", "" + akcja);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return hityCenowes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvHitCenowyNazwa;
        private TextView tvHitCenowyCena;
        private ImageView ivIco;

        public ViewHolder(View itemView) {
            super(itemView);

            tvHitCenowyNazwa = itemView.findViewById(R.id.tvHitCenowyNazwa);
            tvHitCenowyCena = itemView.findViewById(R.id.tvHitCenowyCena);
            ivIco = itemView.findViewById(R.id.ivHitCenowyLogo);

        }
    }
}
