package com.jgcoders.budmaapp.UI.LoginActivity.AktywacjaFragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jgcoders.budmaapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AktywacjaFragment extends Fragment {

    @BindView(R.id.etUid)
    EditText etUid;

    public AktywacjaFragment() {
        // Required empty public constructor
    }

    public static AktywacjaFragment newInstance() {
        AktywacjaFragment fragment = new AktywacjaFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_aktywacja, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.bAktywuj)
    public void onAktywujButtonClicked() {
        String uid = etUid.getText().toString();
        if (uid.isEmpty()) {
            etUid.setError(getString(R.string.uzupelnij));

            return;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
