package com.jgcoders.budmaapp.UI.DrawerFragments.Promocje;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Models.MainActivity.HityCenowe;
import com.jgcoders.budmaapp.Models.Promocje;
import com.jgcoders.budmaapp.Network.Responses.MainActivityJsonResponse;
import com.jgcoders.budmaapp.UI.DrawerFragments.Adapters.HityCenoweDataAdapter;
import com.jgcoders.budmaapp.UI.DrawerFragments.Adapters.PromocjeDataAdapter;
import com.jgcoders.budmaapp.Network.Responses.PromocjeJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PromocjeFragment extends Fragment {

    @Inject
    Retrofit retrofit;

    private RecyclerView rvListaPromocji;

    public PromocjeFragment() {
        // Required empty public constructor
    }


    public static PromocjeFragment newInstance() {
        PromocjeFragment fragment = new PromocjeFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_promocje, container, false);
        ((App) getActivity().getApplication()).getNetworkComponent().inject(this);
        rvListaPromocji = view.findViewById(R.id.rvListaPromocji);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvListaPromocji.setHasFixedSize(true);
        rvListaPromocji.setLayoutManager(layoutManager);
        loadRecyclerViewData();


        return view;
    }

    private void loadRecyclerViewData() {
        RestApi restApi = retrofit.create(RestApi.class);
        Call<MainActivityJsonResponse> call = restApi.getMainActivity();
        call.enqueue(new Callback<MainActivityJsonResponse>() {
            @Override
            public void onResponse(Call<MainActivityJsonResponse> call, Response<MainActivityJsonResponse> response) {
                List<HityCenowe> promocjeList = response.body().getHityCenowe();
                rvListaPromocji.setAdapter(new HityCenoweDataAdapter(promocjeList, getActivity(), R.layout.promocje_row));
            }

            @Override
            public void onFailure(Call<MainActivityJsonResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
