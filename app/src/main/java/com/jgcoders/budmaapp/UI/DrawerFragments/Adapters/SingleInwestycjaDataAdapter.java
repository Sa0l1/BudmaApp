package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.Models.Singleinwestycja;
import com.jgcoders.budmaapp.R;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class SingleInwestycjaDataAdapter extends RecyclerView.Adapter<SingleInwestycjaDataAdapter.ViewHolder> {

    private List<Singleinwestycja> singleHurtowniaProducts;
    private Context context;
    private int rowLayout;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_single_inwestycja, parent, false);
        return new SingleInwestycjaDataAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvInwestycjaName.setText(singleHurtowniaProducts.get(position).getNazwa());
        holder.tvInwestycjaOpis.setText(singleHurtowniaProducts.get(position).getOpis());

        String inwestycjaImage = singleHurtowniaProducts.get(position).getImage();

        Glide.with(context)
                .load(inwestycjaImage)
                .into(holder.ivInwestycjaLogo);

    }

    @Override
    public int getItemCount() {
        return singleHurtowniaProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivInwestycjaLogo;
        private TextView tvInwestycjaName, tvInwestycjaOpis;

        public ViewHolder(View itemView) {
            super(itemView);

            tvInwestycjaName = itemView.findViewById(R.id.tvInwestycjaName);
            tvInwestycjaOpis = itemView.findViewById(R.id.tvInwestycjaOpis);

            ivInwestycjaLogo = itemView.findViewById(R.id.ivInwestycjaLogo);


        }
    }
}
