package com.jgcoders.budmaapp.UI.DrawerFragments.SingleProductActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Network.Responses.SingleProductJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.Hurtownie.ListaHurtowniiProduktuActivity.ListaHurtowniiProduktuActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SingleProductActivity extends AppCompatActivity {

    @Inject
    Retrofit retrofit;

    private TextView tvNazwa, tvNrRef, tvCena, tvOpis, tvSprawdzHurtownie;
    private SliderLayout mSliderLayout;

    private WebView webViewOpis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_product);
        ((App) getApplication()).getNetworkComponent().inject(this);

        tvNazwa = findViewById(R.id.tvSingleProductTitle);
        tvNrRef = findViewById(R.id.tvNrRef);
        tvCena = findViewById(R.id.tvCena);
        //tvOpis = findViewById(R.id.tvOpis);
        webViewOpis = findViewById(R.id.webViewOpis);


        tvSprawdzHurtownie = findViewById(R.id.tvSprawdzHurtownie);
        mSliderLayout = findViewById(R.id.sliderLayout);

        Bundle extras = getIntent().getExtras();
        String akcja = extras.getString("akcja");
        Log.e("intent odebralem akcja", "" + akcja);

        RestApi restApi = retrofit.create(RestApi.class);
        Call<SingleProductJsonResponse> call = restApi.getSingleProduct(akcja);
        call.enqueue(new Callback<SingleProductJsonResponse>() {
            @Override
            public void onResponse(Call<SingleProductJsonResponse> call, Response<SingleProductJsonResponse> response) {
                tvNazwa.setText(response.body().getSingleProduct().getNazwa());
                tvNrRef.setText(response.body().getSingleProduct().getNrRef());
                tvCena.setText(response.body().getSingleProduct().getCena());
                //tvOpis.setText(response.body().getSingleProduct().getOpis());

                webViewOpis.loadData(response.body().getSingleProduct().getOpis(), "text/html; charset=utf-8", "UTF-8");

                final String akcjaDoHurtownii = response.body().getSingleProduct().getAkcjaHurtownii();

                tvSprawdzHurtownie.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(SingleProductActivity.this, ListaHurtowniiProduktuActivity.class);
                        intent.putExtra("akcjaDoHurtownii", akcjaDoHurtownii);
                        Log.e("akcjaDoHurtownii", "" + akcjaDoHurtownii);
                        view.getContext().startActivity(intent);
                    }
                });

                //-----------------slider----------------
                ArrayList<String> listUrl = new ArrayList<>();

                for (int i = 0; i < response.body().getSingleProduct().getZdjecia().size(); i++) {
                    String obrazek = response.body().getSingleProduct().getZdjecia().get(i);
                    listUrl.add(obrazek);
                }
                RequestOptions requestOptions = new RequestOptions();
                requestOptions
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.NONE);

                for (int i = 0; i < listUrl.size(); i++) {
                    TextSliderView sliderView = new TextSliderView(SingleProductActivity.this);
                    // if you want show image only / without description text use DefaultSliderView instead

                    // initialize SliderLayout
                    sliderView.image(listUrl.get(i))
                            //.description(listName.get(i))
                            .setRequestOption(requestOptions)
                            .setBackgroundColor(Color.WHITE)
                            .setProgressBarVisible(false);
                    //.setOnSliderClickListener(getActivity());

                    //add your extra information
                    sliderView.bundle(new Bundle());
                    //sliderView.getBundle().putString("extra", listName.get(i));
                    mSliderLayout.addSlider(sliderView);
                }

                // set Slider Transition Animation
                // mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                mSliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);

                mSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                mSliderLayout.setCustomAnimation(new DescriptionAnimation());
                mSliderLayout.setDuration(4000);
                //mSliderLayout.addOnPageChangeListener(MainActivity);
            }

            @Override
            public void onFailure(Call<SingleProductJsonResponse> call, Throwable t) {

            }
        });
    }
}
