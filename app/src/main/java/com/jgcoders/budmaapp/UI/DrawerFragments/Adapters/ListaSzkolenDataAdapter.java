package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jgcoders.budmaapp.Models.Szkolenia;
import com.jgcoders.budmaapp.R;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class ListaSzkolenDataAdapter extends RecyclerView.Adapter<ListaSzkolenDataAdapter.ViewHolder> {

    private List<Szkolenia> szkoleniaList;
    private Context context;
    private int rowLayout;

    public ListaSzkolenDataAdapter(List<Szkolenia> szkoleniaList, Context context, int rowLayout) {
        this.szkoleniaList = szkoleniaList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public ListaSzkolenDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.szkolenia_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListaSzkolenDataAdapter.ViewHolder holder, int position) {
        holder.tvSzkolenieNazwa.setText(szkoleniaList.get(position).getNazwa());
        holder.tvSzkolenieData.setText(szkoleniaList.get(position).getData());
        holder.tvSzkolenieOpis.setText(szkoleniaList.get(position).getOpis());

    }

    @Override
    public int getItemCount() {
        return szkoleniaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSzkolenieNazwa, tvSzkolenieData, tvSzkolenieOpis;

        public ViewHolder(View itemView) {
            super(itemView);
            tvSzkolenieNazwa = itemView.findViewById(R.id.tvSzkolenieNazwa);
            tvSzkolenieData = itemView.findViewById(R.id.tvSzkolenieData);
            tvSzkolenieOpis = itemView.findViewById(R.id.tvSzkolenieOpis);

        }
    }
}
