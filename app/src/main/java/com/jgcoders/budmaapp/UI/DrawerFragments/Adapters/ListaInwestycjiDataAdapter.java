package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.Models.Listainwestycji;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.InwestycjeFragment.Inwestycje.SingleInwestycjaActivity;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class ListaInwestycjiDataAdapter extends RecyclerView.Adapter<ListaInwestycjiDataAdapter.ViewHolder> {
    private List<Listainwestycji> inwestycjeList;
    private Context context;
    private int rowLayout;

    public ListaInwestycjiDataAdapter(List<Listainwestycji> inwestycjeList, Context context, int rowLayout) {
        this.inwestycjeList = inwestycjeList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inwestycje_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvInwestycjaNazwa.setText(inwestycjeList.get(position).getNazwa());

        String hurtowniaImage = inwestycjeList.get(position).getMiniaturka();
        final String akcja = inwestycjeList.get(position).getAkcja();

        Glide.with(context)
                .load(hurtowniaImage)
                .into(holder.ivInwestycjaImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SingleInwestycjaActivity.class);
                intent.putExtra("akcja", "" + akcja);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return inwestycjeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvInwestycjaNazwa;
        private ImageView ivInwestycjaImage;

        public ViewHolder(View itemView) {
            super(itemView);

            tvInwestycjaNazwa = itemView.findViewById(R.id.tvInwestycjaNazwa);
            ivInwestycjaImage = itemView.findViewById(R.id.ivInwestycjaImage);
        }
    }
}
