package com.jgcoders.budmaapp.UI.DrawerFragments.Producenci.SingleProducent;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Models.Producent.Produkty;
import com.jgcoders.budmaapp.Network.Responses.SingleProducentJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.Adapters.SingleProducentDataAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SingleProducentActivity extends AppCompatActivity {

    @Inject
    Retrofit retrofit;

    private RecyclerView rvListaProduktowProducenta;

    @BindView(R.id.ivProducentLogo)
    ImageView ivProducentLogo;

    @BindView(R.id.tvProducentName)
    TextView tvProducentName;

    @BindView(R.id.tvProducentAddress)
    TextView tvProducentAddress;

    @BindView(R.id.bZadzwonButton)
    Button bZadzwonButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_producent);
        ((App) getApplication()).getNetworkComponent().inject(this);
        ButterKnife.bind(this);

        rvListaProduktowProducenta = findViewById(R.id.rvListaProduktowProducenta);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvListaProduktowProducenta.setHasFixedSize(true);
        rvListaProduktowProducenta.setLayoutManager(layoutManager);
        loadRecyclerViewData();
    }

    private void loadRecyclerViewData() {
        RestApi restApi = retrofit.create(RestApi.class);
        Bundle extras = getIntent().getExtras();
        String akcja = extras.getString("akcja");
        Log.e("intent odebralem akcja", "" + akcja);

        Call<SingleProducentJsonResponse> call = restApi.getSingleProducent(akcja);
        call.enqueue(new Callback<SingleProducentJsonResponse>() {
            @Override
            public void onResponse(Call<SingleProducentJsonResponse> call, Response<SingleProducentJsonResponse> response) {
                Log.e("respo producent single", "" + response.body());
                tvProducentName.setText(response.body().getSingleProducent().getNazwa());
                tvProducentAddress.setText(response.body().getSingleProducent().getAdres());
                final String numerTelefonu = response.body().getSingleProducent().getTelefon();

                Glide.with(SingleProducentActivity.this)
                        .load(response.body().getSingleProducent().getLogo())
                        .into(ivProducentLogo);

                List<Produkty> produktySingleProducent = response.body().getSingleProducent().getProdukty();
                rvListaProduktowProducenta.setAdapter(new SingleProducentDataAdapter(produktySingleProducent, SingleProducentActivity.this, R.layout.single_producent_produkty_row));

                bZadzwonButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + numerTelefonu));
                        if (ActivityCompat.checkSelfPermission(SingleProducentActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(callIntent);

                    }
                });
            }

            @Override
            public void onFailure(Call<SingleProducentJsonResponse> call, Throwable t) {

            }
        });

    }
}
