package com.jgcoders.budmaapp.UI.DrawerFragments.Hurtownie.ListaHurtowniiProduktuActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Models.ListaHurtowni;
import com.jgcoders.budmaapp.UI.DrawerFragments.Adapters.ListaHurtowniiDataAdapter;
import com.jgcoders.budmaapp.Network.Responses.ListaHurtowniJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ListaHurtowniiProduktuActivity extends AppCompatActivity {

    @Inject
    Retrofit retrofit;
    private RecyclerView rvListaHurtownii;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_hurtownii_produktu);
        ((App) getApplication()).getNetworkComponent().inject(this);

        rvListaHurtownii = findViewById(R.id.rvListaHurtownii);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvListaHurtownii.setHasFixedSize(true);
        rvListaHurtownii.setLayoutManager(layoutManager);

        loadRecyclerViewData();
    }

    private void loadRecyclerViewData() {
        Bundle extras = getIntent().getExtras();
        String akcja = extras.getString("akcjaDoHurtownii");
        Log.e("intent extra akcja", "" + akcja);

        RestApi restApi = retrofit.create(RestApi.class);
        Call<ListaHurtowniJsonResponse> call = restApi.getListaHurtownii();
        call.enqueue(new Callback<ListaHurtowniJsonResponse>() {
            @Override
            public void onResponse(Call<ListaHurtowniJsonResponse> call, Response<ListaHurtowniJsonResponse> response) {
                List<ListaHurtowni> listaHurtowniList = response.body().getListaHurtownii();
                rvListaHurtownii.setAdapter(new ListaHurtowniiDataAdapter(listaHurtowniList, getApplicationContext(), R.layout.hurtownie_row));
            }

            @Override
            public void onFailure(Call<ListaHurtowniJsonResponse> call, Throwable t) {

            }
        });
    }
}
