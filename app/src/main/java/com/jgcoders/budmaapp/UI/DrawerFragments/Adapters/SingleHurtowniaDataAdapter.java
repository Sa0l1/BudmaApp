package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.Models.Producent.Produkty;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.SingleProductActivity.SingleProductActivity;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class SingleHurtowniaDataAdapter extends RecyclerView.Adapter<SingleHurtowniaDataAdapter.ViewHolder> {
    private List<Produkty> singleHurtowniaProducts;
    private Context context;
    private int rowLayout;

    public SingleHurtowniaDataAdapter(List<Produkty> singleHurtowniaProducts, Context context, int rowLayout) {
        this.singleHurtowniaProducts = singleHurtowniaProducts;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public SingleHurtowniaDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_hurtownia_product_row, parent, false);

        return new SingleHurtowniaDataAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SingleHurtowniaDataAdapter.ViewHolder holder, int position) {
        holder.tvProduktNazwa.setText(singleHurtowniaProducts.get(position).getNazwa());
        holder.tvProduktCena.setText(singleHurtowniaProducts.get(position).getCena());
        String ivIco = singleHurtowniaProducts.get(position).getZdjecie();

        final String akcja = singleHurtowniaProducts.get(position).getAkcja();

        Glide.with(context)
                .load(ivIco)
                .into(holder.ivProduktLogo);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SingleProductActivity.class);
                intent.putExtra("akcja", "" + akcja);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return singleHurtowniaProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvProduktNazwa, tvProduktCena;
        private ImageView ivProduktLogo;

        public ViewHolder(View itemView) {
            super(itemView);

            tvProduktNazwa = itemView.findViewById(R.id.tvNazwa);
            tvProduktCena = itemView.findViewById(R.id.tvCena);

            ivProduktLogo = itemView.findViewById(R.id.ivProductImage);
        }
    }
}
