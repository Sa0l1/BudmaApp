package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.Models.Promocje;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.SingleProductActivity.SingleProductActivity;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class PromocjeDataAdapter extends RecyclerView.Adapter<PromocjeDataAdapter.ViewHolder> {

    private List<Promocje> promocjeList;
    private Context context;
    private int rowLayout;

    public PromocjeDataAdapter(List<Promocje> promocjeList, Context context, int rowLayout) {
        this.promocjeList = promocjeList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.promocje_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvPromocjeNazwa.setText(promocjeList.get(position).getNazwa());
        holder.tvPromocjeCena.setText(promocjeList.get(position).getCena());
        String ivIco = promocjeList.get(position).getMiniaturka();
        final String akcja = promocjeList.get(position).getAkcja();

        Glide.with(context)
                .load(ivIco)
                .into(holder.ivPromocjeImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SingleProductActivity.class);
                intent.putExtra("akcja", "" + akcja);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return promocjeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvPromocjeNazwa, tvPromocjeCena;
        private ImageView ivPromocjeImage;

        public ViewHolder(View itemView) {
            super(itemView);

            tvPromocjeNazwa = itemView.findViewById(R.id.tvPromocjeNazwa);
            tvPromocjeCena = itemView.findViewById(R.id.tvPromocjeCena);
            ivPromocjeImage = itemView.findViewById(R.id.ivPromocjeImage);

        }
    }
}
