package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.Models.Producent.ListaProducentowModel;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.Producenci.SingleProducent.SingleProducentActivity;

import java.util.List;

/**
 * Created by krzysztofm on 26.01.2018.
 */

public class ProducenciListDataAdapter extends RecyclerView.Adapter<ProducenciListDataAdapter.ViewHolder> {

    private List<ListaProducentowModel> producentowModelList;
    private Context context;
    private int rowLayout;

    public ProducenciListDataAdapter(List<ListaProducentowModel> producentowModelList, Context context, int rowLayout) {
        this.producentowModelList = producentowModelList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.producent_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvProducentName.setText(producentowModelList.get(position).getNazwa());
        String ivProducentLogo = producentowModelList.get(position).getLogo();

        final String akcja = producentowModelList.get(position).getAkcja();

        Glide.with(context)
                .load(ivProducentLogo)
                .into(holder.ivProducentLogo);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SingleProducentActivity.class);
                intent.putExtra("akcja", "" + akcja);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return producentowModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvProducentName;
        private ImageView ivProducentLogo;

        public ViewHolder(View itemView) {
            super(itemView);

            tvProducentName = itemView.findViewById(R.id.tvProducentName);
            ivProducentLogo = itemView.findViewById(R.id.ivProducentLogo);
        }
    }
}
