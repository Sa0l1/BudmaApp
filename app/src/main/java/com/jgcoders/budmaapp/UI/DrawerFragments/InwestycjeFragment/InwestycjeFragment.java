package com.jgcoders.budmaapp.UI.DrawerFragments.InwestycjeFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Models.Listainwestycji;
import com.jgcoders.budmaapp.UI.DrawerFragments.Adapters.ListaInwestycjiDataAdapter;
import com.jgcoders.budmaapp.Network.Responses.ListaInwestycjiJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class InwestycjeFragment extends Fragment {

    @Inject
    Retrofit retrofit;

    private RecyclerView rvListaInwestycje;

    public InwestycjeFragment() {
        // Required empty public constructor
    }


    public static InwestycjeFragment newInstance() {
        InwestycjeFragment fragment = new InwestycjeFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inwestycje, container, false);
        ((App) getActivity().getApplication()).getNetworkComponent().inject(this);

        rvListaInwestycje = view.findViewById(R.id.rvListaInwestycji);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvListaInwestycje.setHasFixedSize(true);
        rvListaInwestycje.setLayoutManager(layoutManager);
        loadRecyclerViewData();

        return view;
    }

    private void loadRecyclerViewData() {
        RestApi restApi = retrofit.create(RestApi.class);
        Call<ListaInwestycjiJsonResponse> call = restApi.getListaInwestycji();
        call.enqueue(new Callback<ListaInwestycjiJsonResponse>() {
            @Override
            public void onResponse(Call<ListaInwestycjiJsonResponse> call, Response<ListaInwestycjiJsonResponse> response) {
                Log.e("response", "" + response.body());
                List<Listainwestycji> listainwestycji = response.body().getListainwestycji();
                rvListaInwestycje.setAdapter(new ListaInwestycjiDataAdapter(listainwestycji, getActivity().getApplicationContext(), R.layout.inwestycje_row));

            }

            @Override
            public void onFailure(Call<ListaInwestycjiJsonResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
