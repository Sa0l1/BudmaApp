package com.jgcoders.budmaapp.UI.DrawerFragments.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.Models.ListaHurtowni;
import com.jgcoders.budmaapp.R;
import com.jgcoders.budmaapp.UI.DrawerFragments.Hurtownie.SingleHurtownia.SingleHurtowniaActivity;

import java.util.List;

/**
 * Created by staszekalcatraz on 28/01/2018.
 */

public class ListaHurtowniiDataAdapter extends RecyclerView.Adapter<ListaHurtowniiDataAdapter.ViewHolder> {
    private List<ListaHurtowni> hurtownieList;
    private Context context;
    private int rowLayout;

    public ListaHurtowniiDataAdapter(List<ListaHurtowni> hurtownieList, Context context, int rowLayout) {
        this.hurtownieList = hurtownieList;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hurtownie_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvHurtownieNazwa.setText(hurtownieList.get(position).getNazwa());
        holder.tvHurtownieLokalizacja.setText(hurtownieList.get(position).getLokalizacja());

        String hurtowniaImage = hurtownieList.get(position).getMiniaturka();
        final String akcja = hurtownieList.get(position).getAkcja();

        Glide.with(context)
                .load(hurtowniaImage)
                .into(holder.ivHurtowniaImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SingleHurtowniaActivity.class);
                intent.putExtra("akcja", "" + akcja);
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return hurtownieList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvHurtownieNazwa, tvHurtownieLokalizacja;
        private ImageView ivHurtowniaImage;

        public ViewHolder(View itemView) {
            super(itemView);
            tvHurtownieNazwa = itemView.findViewById(R.id.tvHurtowniaNazwa);
            tvHurtownieLokalizacja = itemView.findViewById(R.id.tvHurtownieLokalizacja);

            ivHurtowniaImage = itemView.findViewById(R.id.ivHurtowniaImage);
        }
    }
}
