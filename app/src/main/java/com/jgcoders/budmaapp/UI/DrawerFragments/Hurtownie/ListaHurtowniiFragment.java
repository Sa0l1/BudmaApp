package com.jgcoders.budmaapp.UI.DrawerFragments.Hurtownie;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Models.ListaHurtowni;
import com.jgcoders.budmaapp.UI.DrawerFragments.Adapters.ListaHurtowniiDataAdapter;
import com.jgcoders.budmaapp.Network.Responses.ListaHurtowniJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class ListaHurtowniiFragment extends Fragment {

    @Inject
    Retrofit retrofit;

    private RecyclerView rvListaHurtownii;

    public ListaHurtowniiFragment() {
        // Required empty public constructor
    }

    public static ListaHurtowniiFragment newInstance() {
        ListaHurtowniiFragment fragment = new ListaHurtowniiFragment();


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((App) getActivity().getApplication()).getNetworkComponent().inject(this);


        View view = inflater.inflate(R.layout.fragment_hurtownie, container, false);
        rvListaHurtownii = view.findViewById(R.id.rvListaHurtownii);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvListaHurtownii.setHasFixedSize(true);
        rvListaHurtownii.setLayoutManager(layoutManager);
        loadRecyclerViewData();

        return view;
    }

    private void loadRecyclerViewData() {
        RestApi restApi = retrofit.create(RestApi.class);
        Call<ListaHurtowniJsonResponse> call = restApi.getListaHurtownii();
        call.enqueue(new Callback<ListaHurtowniJsonResponse>() {
            @Override
            public void onResponse(Call<ListaHurtowniJsonResponse> call, Response<ListaHurtowniJsonResponse> response) {
                List<ListaHurtowni> listaHurtowniList = response.body().getListaHurtownii();
                rvListaHurtownii.setAdapter(new ListaHurtowniiDataAdapter(listaHurtowniList, getActivity(), R.layout.hurtownie_row));
            }

            @Override
            public void onFailure(Call<ListaHurtowniJsonResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
