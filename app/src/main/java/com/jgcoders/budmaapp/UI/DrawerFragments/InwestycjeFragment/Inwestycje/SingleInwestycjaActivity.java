package com.jgcoders.budmaapp.UI.DrawerFragments.InwestycjeFragment.Inwestycje;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jgcoders.budmaapp.App;
import com.jgcoders.budmaapp.Network.Responses.SingleInwestycjaJsonResponse;
import com.jgcoders.budmaapp.Network.RestApi;
import com.jgcoders.budmaapp.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SingleInwestycjaActivity extends AppCompatActivity {

    @Inject
    Retrofit retrofit;

    @BindView(R.id.bZadzwonButton)
    Button bZadzwonButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_inwestycja);
        ((App) getApplication()).getNetworkComponent().inject(this);


        ButterKnife.bind(this);

        loadRecyclerViewData();
    }

    private void loadRecyclerViewData() {
        RestApi restApi = retrofit.create(RestApi.class);
        Bundle extras = getIntent().getExtras();
        String akcja = extras.getString("akcja");
        Log.e("intent extra akcja", "" + akcja);

        Call<SingleInwestycjaJsonResponse> call = restApi.getSingleInwestycja(akcja);
        call.enqueue(new Callback<SingleInwestycjaJsonResponse>() {
            @Override
            public void onResponse(Call<SingleInwestycjaJsonResponse> call, Response<SingleInwestycjaJsonResponse> response) {
                ImageView ivInwestycjaLogo = findViewById(R.id.ivInwestycjaLogo);
                TextView tvInwestycjaName = findViewById(R.id.tvInwestycjaName);
                TextView tvInwestycjaOpis = findViewById(R.id.tvInwestycjaOpis);


                tvInwestycjaName.setText(response.body().getSingleinwestycja().getNazwa());
                tvInwestycjaOpis.setText(response.body().getSingleinwestycja().getOpis());
                final String numerTelefonu = response.body().getSingleinwestycja().getTelefon();

                Glide.with(SingleInwestycjaActivity.this)
                        .load(response.body().getSingleinwestycja().getImage())
                        .into(ivInwestycjaLogo);

                bZadzwonButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + numerTelefonu));
                        if (ActivityCompat.checkSelfPermission(SingleInwestycjaActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(callIntent);

                    }
                });
            }

            @Override
            public void onFailure(Call<SingleInwestycjaJsonResponse> call, Throwable t) {
                Log.e("singleinwestycja", "" + t.getMessage());
            }
        });
    }
}
