package com.jgcoders.budmaapp.dagger2.Components;

import com.jgcoders.budmaapp.UI.DrawerFragments.Aktualnosci.AktualnosciFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.Hurtownie.ListaHurtowniiFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.Hurtownie.ListaHurtowniiProduktuActivity.ListaHurtowniiProduktuActivity;
import com.jgcoders.budmaapp.UI.DrawerFragments.Hurtownie.SingleHurtownia.SingleHurtowniaActivity;
import com.jgcoders.budmaapp.UI.DrawerFragments.InwestycjeFragment.InwestycjeFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.Producenci.ProducenciFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.Producenci.SingleProducent.SingleProducentActivity;
import com.jgcoders.budmaapp.UI.DrawerFragments.Promocje.PromocjeFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.SingleProductActivity.SingleProductActivity;
import com.jgcoders.budmaapp.UI.DrawerFragments.Szkolenia.SzkoleniaFragment;
import com.jgcoders.budmaapp.UI.DrawerFragments.InwestycjeFragment.Inwestycje.SingleInwestycjaActivity;
import com.jgcoders.budmaapp.UI.MainActivity;
import com.jgcoders.budmaapp.dagger2.Modules.AppModule;
import com.jgcoders.budmaapp.dagger2.Modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by krzysztofm on 26.01.2018.
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface NetworkComponent {
    void inject(MainActivity activity);

    void inject(ProducenciFragment fragment);

    void inject(PromocjeFragment fragment);

    void inject(AktualnosciFragment fragment);

    void inject(SzkoleniaFragment fragment);

    void inject(ListaHurtowniiFragment fragment);

    void inject(InwestycjeFragment fragment);

    void inject(SingleProductActivity activity);

    void inject(SingleProducentActivity activity);

    void inject(SingleHurtowniaActivity activity);

    void inject(SingleInwestycjaActivity activity);

    void inject(ListaHurtowniiProduktuActivity activity);
}
